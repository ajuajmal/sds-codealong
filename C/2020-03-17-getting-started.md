---
title: Getting Started with C
subtitle: Installing the GCC Compiler
date: 2020-03-17
tags: ["gcc", "c", "compiler"]
---

# Geting Started with C
**Reference Resources**

-   C Programming Language, 2nd Edition by Brian W. Kernighan and Dennis M. Ritchie
-   [C Tutorials](https://www.tutorialspoint.com/cprogramming/index.htm) by Tutorial's point


<a id="org11e18f2"></a>

# Installing the C compiler

A compiler is basically what reads the code you have written and turns it into 1s and 0s, so that physical hardware such as processors and ICs can understand and execute the code you've written. If you already have a compiler installed skip this step. If you have already installed an IDE like CodeBlocks chances are the compiler is installed too. 

If not, follow the steps shown in this [video](https://www.youtube.com/watch?v=sXW2VLrQ3Bs) if you are on a windows PC. 

If you're on a Linux system open the terminal and run one of the commands below based on your OS:

-   Ubuntu/Debian

    sudo apt-get install gcc

-   Arch Linux

    sudo pacman -S gcc

-   Fedora

    sudo yum install gcc


<a id="orgb0c6b0f"></a>

# Installing a text editor

This is often a point of controversy, my advice here is to use whatever you're comfortable with or whatever is being used in your classes at your school or college. That being said I would personally suggest using [Visual Studio Code](https://code.visualstudio.com/) which in my opinion is a much better text editor and can be used as an IDE for multiple languages.

To install vs code follow the steps show in this [video](https://www.youtube.com/watch?v=8tkuu0Rugg4) if you're on windows.

On follow on of the following steps based on your distribution:

-   Ubuntu
    Follow the instructions in this [video](https://www.youtube.com/watch?v=mfxP0REDWs4)
-   Arch Linux
    Execute in terminal:
    
        sudo pacman -S code
-   Fedora
    Follow the steps from this [link](https://tecadmin.net/install-visual-studio-code-editor-in-fedora/) or search your app-store.


<a id="org359fcea"></a>

# Running Code in VS Code

You can run code within C++ itself just like in codeblocks. To enable the functionality follow the steps from this [video](https://www.youtube.com/watch?v=tQTrs5G0H5c&feature=youtu.be)

Now you can write and run code in vs code. :)


<a id="org1b5465a"></a>

# Relevant Tutorials:

-   [C Programming Tutorials](https://www.youtube.com/watch?v=iWx3yyFMWQA&index=3&list=PL6gx4Cwl9DGAKIXv8Yr6nhGJ9Vlcjyymq) by [thenewboston](https://www.youtube.com/channel/UCJbPGzawDH1njbqV-D5HqKw)
-   [C Programming Tutorial for Absolute Beginners](https://www.udemy.com/c-programming-tutorial-for-absolute-beginners/) in [Udemy](https://www.udemy.com/)

