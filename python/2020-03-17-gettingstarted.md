---
title: Getting Started with Python
subtitle: Installing Python
date: 2020-03-17
tags: ["python", "vscode", "interpreter"]
---

**Reference Resources**

-  http://wiki.python.org/moin/


<a id="org11e18f2"></a>

# Installing Python 3 interpreter

Interpreters are program that reads Python programs and carries out their instructions; you need it before you can do any Python programming. Mac and Linux distributions may include an outdated version of Python (Python 2), but you should install an updated one (Python 3).

- Windows Users
    Download [here](https://python.org/downloads/windows/)



If you're on a Linux system open the terminal and run one of the commands below based on your OS:

-   Ubuntu/Debian

    `sudo apt-get install python3`

-   Arch Linux

    `sudo pacman -Sy python3`

-   Fedora

    `sudo yum install python3`


<a id="orgb0c6b0f"></a>

# Installing a text editor

This is often a point of controversy, my advice here is to use whatever you're comfortable with or whatever is being used in your classes at your school or college. That being said I would personally suggest using [Visual Studio Code](https://code.visualstudio.com/) which in my opinion is a much better text editor and can be used as an IDE for multiple languages.

To install vs code follow the steps show in this [video](https://www.youtube.com/watch?v=8tkuu0Rugg4) if you're on windows.

On follow on of the following steps based on your distribution:

-   Ubuntu
    Follow the instructions in this [video](https://www.youtube.com/watch?v=mfxP0REDWs4)
-   Arch Linux
    Execute in terminal:
`sudo pacman -S code`
-   Fedora
    Follow the steps from this [link](https://tecadmin.net/install-visual-studio-code-editor-in-fedora/) or search your app-store.


<a id="org359fcea"></a>

# Running Code in VS Code

You can run code within C++ itself just like in codeblocks. To enable the functionality follow the steps from this [video](https://www.youtube.com/watch?v=tQTrs5G0H5c&feature=youtu.be)

Now you can write and run code in vs code. :)


<a id="org1b5465a"></a>

# Referrals:

-   [Official Python Documentation](https://docs.python.org/3/)
-   [Practice resource](http://www.practicepython.org/)
-    Video tutorials: [Udemy](https://www.udemy.com/python-for-absolute-beginners-u/) 
    [Udacity](https://www.udacity.com/course/programming-foundations-with-python--ud036)
    [Cousera](https://www.coursera.org/learn/python3)

-   [Python Wiki](http://wiki.python.org/)