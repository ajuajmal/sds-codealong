---
title: About
subtitle: Contributing to this project
date: 2020-03-17
tags: ["contributing", "about"]
---

# SDS Code Along
If you wish to contribute this project, please fork this repo and add your content in a folder with the name of the technology you're creating content for. So if you're creating content for python the name of the folder should be python. Divide the content into multiple files. On the blog each file will be a post. Once you've finished a file open a pull request and the admins will merge it asap :)

### Things to remember
- Name the file should be `yyyy-mm-dd-name-of-the-file.md`.
- Inculde the metadata in the file.
```yaml
---
title: <TITLE>
subtitle: <SUBTITLE>
date: <TAG>
tags: ["tag1", "tag2"]
---
```
- Stick to Markdown.
- Explain properly but do not scare people away.
- Break the content into files! Nobody has time for long articles.
- When submitting PR expalain what you've added.
- Keep things PG-13.
- When in doubt as for help. You guys can ping me @avronr on telegram for now.