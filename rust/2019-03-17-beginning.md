---
title: Rusty Beginings
subtitle: Introduction to Rust
date: 2020-03-17
tags: ["rust", "rustc", "compiler"]
---

# Rusty Beginnings

Hey there, so you wanna become a rustacean? I see you are interested in rust-lang and want to understand the madness behind the magic that makes it a safe, speedy and concurrent language to write software in. So it's always safe to understand why you should learn it in the first place, lemme give you a quick pitch!

## Why learn rust?

[Rust is the language of the next 40 years](https://gitlab.com/carols10cents/rust-next-40-years), don't take my word for it, check out the talk! It is built to alleviate a problem that ~99% of all software developers face when writiing low-level and performant code. But it is also being developed to scratch common itches and unsafe programming practices, such as building in a Garbage Collector to manage memory, which languages such as Python and Golang do!

Rust-lang on the other hand manages memory by making use of a complex borrow-checker mechanism that does it by following your commands as given in the program. This is also done at compile time, so you don't face the issue when you are running your program, unlike with Go. It's also a good language to use instead of C/C++, because it has much more robust features and is very much suitable for implementing many Data Structures and Algorithms(There are a few data structures that can't be, but let's consider them outliers in our case). Anyways, there are multiple reasons why you should consider using rust instead of any other language for writing low-level code, just do an internet search and you'll know why.

## Installing rust

If you are going to follow the codealong, it is highly likely you will be writing code and so [installing rust](https://rust-lang.org/tools/install) or using the [playground](https://play.rust-lang.org) are the two options you have to get started with the same. Most things are possible with the playground, but I would still suggest you use [rustup](https://rustup.rs) to get started, also if you are [on Windows, use this](https://forge.rust-lang.org/infra/other-installation-methods.html)(But it is highly recommended to use a *nix based OS).


## Writing your first rust program

So, you installed rust and now you are ready to write your first program. Rust programs, much like C programs, are directly converted into binaries by the rust compiler(`rustc`). Similarly, like NPM is for JS, we use `cargo` to manage the packages we create or install from other repositories. Packages are the source-code being used to create a binary or libraries(Code that can be reused in other programs). So let's make a package for ourselves to get started!

```bash
cargo init codealong
```

Which will create a folder named `codealong` in the current directory. This new folder will also contain the `Config.toml` file and a `src/` directory. The `Config.toml` document is very much your package's manifesto and contains relevant info like version number, dependencies and more. Edit this file to make changes to the package details. In the `src/` directory, you will find the `main.rs` file, within which lies the code that we are most interested in. Let's go through it:

```rust
fn main() {
	println!("Hello, World!");
}
```

This is similar to the following C program:
```c
#include <stdio.h>
void main()
{
	printf("Hello, World!\n");
}
```

You can run the above rust program with the following command:
```bash
cargo run
```

That was very simple, wasn't it? We just went through writing a Hello World program in Rust, but that's just the starting and there's more to come up!
